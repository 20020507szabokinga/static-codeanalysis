﻿using AnalyzableCodes.Bugs;
using AnalyzableCodes.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalyzableCodes
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Név: ");
                var name = Console.ReadLine();
                UsernameProvider usernameProvider = new UsernameProvider();
                if (!string.IsNullOrEmpty(name))
                {
                    if (name != "bye")
                    {
                        if (name != "quit")
                        {
                            usernameProvider.Fullname = name;
                        }
                        else
                        {
                            Environment.Exit(0);
                        }
                    }
                    else
                    {
                        Environment.Exit(0);
                    }
                }
                Console.WriteLine("Jelszó: ");
                var password = Console.ReadLine();
                if (!string.IsNullOrEmpty(password))
                {
                    if (password.Length >= 8)
                    {
                        usernameProvider.Password = password;
                    }
                    else
                    {
                        Console.WriteLine("Nem megfelelő a jelszó");
                    }
                }


                Console.WriteLine(usernameProvider);
                Console.ReadKey();

            }


        }



    }
}
